# Calculer la somme des éléments d'un tableau.

Écrire une fonction `somme` qui prend en paramètre un tableau `t` et qui renvoie la somme de ses éléments.

{{ IDE('exo1', SANS='sum') }}